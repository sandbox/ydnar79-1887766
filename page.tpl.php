      <?php if ($title): ?>
        <h1 class="title" id="page-title">
          <?php print $title; ?>
        </h1>
      <?php endif; ?>
<div id="messages"><?php print $messages; ?></div>
<div id="content-area"><?php print render($page['content']); ?></div>
